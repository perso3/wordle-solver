
import random

from functions import *


def load_words(file_name):
    word_list = []
    with open(file_name, "r") as f:
        for line in f.readlines():
            word_list.append(line.replace("\n", ""))

    return word_list


if __name__ == '__main__':
    words = load_words("words.txt")

    new_words = words
    attempts = 0
    while len(new_words) != 1:
        print(new_words)
        user_input = random.choice(new_words)
        print(f'Try -> {user_input}')
        correct = input_to_list(input("Correct letters (exemple : c0 r1 u2 e3 l4) : "))
        present = input_to_list(input("Present letters (exemple : c1 r2 u3 e4 l0) : "))
        ignore = input_to_list(input("Ignored letters (exemple : a b d s) : "))

        print(correct)
        print(present)
        print(ignore)

        new_words = letters_at(correct, new_words)
        new_words = contains(present, new_words)
        new_words = without_letters_at(present, new_words)
        new_words = without_letters(ignore, new_words)
        attempts += 1
    print(new_words[0])

# if __name__ == '__main__':
#     words = load_words("words.txt")
#
#     word = random.choice(words)
#     print(f"{word=}")
#     new_words = words
#
#     attempts = 0
#
#     while len(new_words) != 1:
#
#         user_input = random.choice(new_words)
#         correct, present, ignore = verify_input(user_input, word)
#         print(f"{user_input=}")
#         print(f"{correct=}")
#         print(f"{present=}")
#         print(f"{ignore=}")
#         new_words = contains(present, new_words)
#         new_words = without_letters_at(present, new_words)
#         new_words = letters_at(correct, new_words)
#         new_words = without_letters(ignore, new_words)
#
#         attempts += 1
#
#     print(f"{attempts=}")

# if __name__ == '__main__':
#
#     words = load_words("words.txt")
#
#     total_attempts = []
#     for i in range(500):
#         word = random.choice(words)
#         print(f"{word=}")
#         new_words = words
#
#         attempts = 0
#
#         while len(new_words) > 1:
#
#             user_input = random.choice(new_words)
#             correct, present, ignore = verify_input(user_input, word)
#
#             new_words = contains(present, new_words)
#             new_words = without_letters_at(present, new_words)
#             new_words = letters_at(correct, new_words)
#             new_words = without_letters(ignore, new_words)
#
#             attempts += 1
#
#         print(f"{attempts=}")
#         total_attempts.append(attempts)
#
#     print(sum(total_attempts)/len(total_attempts))

# u1 m2 o3 r4
# o1 r2 o4
# c n i c t b