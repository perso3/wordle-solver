def verify_input(user_input, word):
    correct = []
    present = []
    ignore = []

    for i, letter in enumerate(user_input):
        if letter == word[i]:
            correct.append([letter, i])
        elif letter in word:
            present.append([letter, i])
        else:
            ignore.append(letter)

    return correct, present, ignore


def prob_letter_at(letter, index, words):
    count = 0
    for word in words:
        if word[index] == letter:
            count += 1

    return count / len(words)


def without_letters_at(letters, words):
    word_list = words

    for word in words:
        for letter in letters:
            if letter[0] == word[letter[1]]:
                if word in word_list:
                    word_list.remove(word)

    return word_list


def without_letters(letters, words):
    word_list = []

    for word in words:
        contains_a_letter = False
        for letter in letters:
            if letter[0] in word:
                contains_a_letter = True

        if not contains_a_letter:
            word_list.append(word)

    return word_list


def letters_at(letters, words):
    word_list = []

    for word in words:
        contains_all_letters = True
        for letter in letters:
            if letter[0] != word[letter[1]]:
                contains_all_letters = False
                break

        if contains_all_letters:
            word_list.append(word)

    return word_list


def contains(letters, words):
    word_list = []

    for word in words:
        contains_all_letters = True
        for letter in letters:
            if letter[0] not in word:
                contains_all_letters = False
                break

        if contains_all_letters:
            word_list.append(word)

    return word_list


def input_to_list(user_input):
    result = []
    if len(user_input) != 0:
        for element in user_input.split(" "):
            if len(element) > 1:
                result.append([element[0], int(element[1])])
            else:
                result.append(element)

    return result